import { connect } from "react-redux";

function Header(props) {
    const {name, link, onlineMemberCount } = props;

    var displayServer = "no-server"
    if (link) {
        displayServer = "has-server";
    }

    return (
        <div className="widget--header">
            <a className="widget--logo" href="https://discord.com/" target="_blank" rel="noreferrer"></a>
            <div className={`server ${displayServer}`}>
                <a href={link} target="_blank" rel="noreferrer"><span className="name">{name}</span></a>
                <span className="count"><strong>{onlineMemberCount ? onlineMemberCount : 0}</strong> Members Online</span>
            </div>
        </div>
    );
}

const mapStateToProps = store => ({
    name: store.server.name,
    link: store.server.link,
    onlineMemberCount: store.members.length
});

export default connect(mapStateToProps)(Header);