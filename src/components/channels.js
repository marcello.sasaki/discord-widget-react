import { connect } from "react-redux";

function Channels(props) {
    var displayChannels = "no-channels"
    if (props.channels.length > 0) {
        displayChannels = "has-channels"
    }

    return (
        <div className={`channels ${displayChannels}`}>
            <div className="channels--header">Channels</div>
            <div className="channels--body">
                {props.channels.map(channel =>
                    <div className="channel" key={channel.id}>
                        <div className="channel--name">{channel.name}</div>
                        <div>
                            {channel.members.map(member =>
                                <div className="user" key={member.id}>
                                    <div className="image--wrap">
                                        <img className="user--image" alt="" src={member.image} />
                                    </div>
                                    <span className="username">{member.name}</span>
                                    {member.mute ? (<span className="icon icon--mute" />) : <></>}
                                    {member.deaf ? (<span className="icon icon--deaf" />) : <></>}
                                </div>
                            )}
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}

const mapStateToProps = store => ({
    channels: store.channels
});

export default connect(mapStateToProps)(Channels);