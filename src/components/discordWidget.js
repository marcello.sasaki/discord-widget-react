import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchGuildWidget } from "../actions";

import Channels from "./channels";
import Footer from "./footer";
import Header from "./header";
import Members from "./members";

class DiscordWidget extends Component {
    componentDidMount() {
        if (this.props.serverId) {
            console.log("using custom server ID");
            this.props.fetchGuildWidget(this.props.serverId);
        } else if (this.props.defaultServerId) {
            console.log("using default server ID");
            this.props.fetchGuildWidget(this.props.defaultServerId);
        }
    }

    render() {

        const { lightMode } = this.props;

        const widgetTheme = `widget widget-theme-seaproxime ${lightMode ? "widget-theme-light" : "widget-theme-dark"}`;
        return (
            <div className={widgetTheme}>

                <Header />

                <div className="widget--body">
                    <Members />

                    <hr />

                    <Channels />
                </div>

                <Footer />

            </div>
        );
    }
}

const mapStateToProps = store => ({
    defaultServerId: store.configurations.serverId,
    lightMode: store.configurations.lightMode
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({ fetchGuildWidget }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DiscordWidget);