import { connect } from "react-redux";

function Members(props) {
    var displayUsers = "no-users"
    if (props.members.length > 0) {
        displayUsers = "has-users"
    }

    return (
        <div className={`users ${displayUsers}`}>
            <div className="users--header">Online Users</div>
            <div className="users--body">
                {props.members.map((user) =>
                    <div className="user" key={user.id}>
                        <div className="image--wrap">
                            <div className={`user--status status--${user.status}`} />
                            <img className="user--image" src={user.image} alt="" />
                        </div>
                        <span className="username">{user.name}</span>
                        <span className="user--game">{user.game}</span>
                    </div>
                )}
            </div>
        </div>
    );
}

const mapStateToProps = store => ({
    members: store.members
});

export default connect(mapStateToProps)(Members);