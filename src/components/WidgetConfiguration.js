import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { setServerId } from "../actions/index";

class WidgetConfiguration extends Component {
    state = {
        guildId: ""
    }

    guildIdInputChange = event => {
        this.setState({
            guildId: event.target.value
        })
    }

    componentDidMount() {
        if (this.props.currentServerId) {
            this.setState({
                guildId: this.props.currentServerId
            })
        }
    }

    render() {
        const { currentServerId, setServerId } = this.props;
        const newServerId = this.state.guildId;

        return (
            <header className="App-header">
                <div className="loading" id="image">
                    <center>
                        <img src="https://cdn.discordapp.com/attachments/414258067870449665/445736475158380544/discord.gif" alt="" />
                    </center>
                </div>
                <div>
                    <label htmlFor="guild_id">Discord Server ID</label>
                    <input
                        type="text"
                        name="guild_id"
                        onChange={this.guildIdInputChange}
                        value={this.state.guildId}
                        id="guild_id"
                    />
                </div>
                <button 
                    onClick={() => setServerId(this.state.guildId)}
                    disabled={!newServerId || newServerId === currentServerId}
                >
                    Save
                </button>
            </header>
        );
    }
}

const mapStateToProps = store => ({
    currentServerId: store.configurations.serverId
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({ setServerId }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WidgetConfiguration);