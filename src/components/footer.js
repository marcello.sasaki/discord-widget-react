import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { enableLightMode } from "../actions";

function Footer({link, lightMode, enableLightMode}) {
    return (
        <div className="widget--footer">
            <span className="info">Free voice chat from Discord</span>
            {link ? (<a className="widget--buttonConnect" href={link} target="_blank" rel="noreferrer"><i class="lab la-discord"></i></a>) : <></>}
            <div className="themeToggler">
                <input
                    type="checkbox"
                    className="themeToggler--input"
                    checked={lightMode}
                    onChange={(event) => enableLightMode(event.target.checked)}
                    id="themeTogglerInput"
                />
                <label className="themeToggler--label" htmlFor="themeTogglerInput">
                    <i className="las la-sun"></i>
                    <i className="las la-moon"></i>
                    <div className="themeToggler--slider"></div>
                </label>
            </div>
        </div>
    );
}

const mapStateToProps = store => ({
    link: store.server.link,
    lightMode: store.configurations.lightMode
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({ enableLightMode }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Footer);