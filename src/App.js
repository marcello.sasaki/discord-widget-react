import { 
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";

import './App.css';

import * as paths from "./constants/routes";
import DiscordWidget from "./components/discordWidget";
import WidgetConfiguration from './components/WidgetConfiguration';

function App() {
  return (
    <HashRouter>
      <div className="App">
        <Route exact path={paths.HOME} component={DiscordWidget} />
        <Route exact path={paths.CONFIGURATIONS} component={WidgetConfiguration} />
      </div>
    </HashRouter>
  );
}

export default App;
