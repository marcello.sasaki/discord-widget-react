import axios from 'axios';

export default axios.create({
    baseURL: "https://discord.com/api"
})

export const GET_GUILD_WIDGET_PATH = (guild_id) => `/guilds/${guild_id}/widget.json`;
