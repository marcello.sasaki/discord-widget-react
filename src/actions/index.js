import * as types from "./actionTypes";
import discord, { GET_GUILD_WIDGET_PATH } from "../apis/discord";



export const fetchGuildWidget = guild_id => async dispatch => {
    try {
        const response = await discord.get(GET_GUILD_WIDGET_PATH(guild_id));
        dispatch({
            type: types.FETCH_GUILD_WIDGET,
            payload: response.data
        });
    } catch (e) {
        console.debug(`${e.response.status}: ${e.response.status.text}`);
        console.debug(`Error getting Discord widget data for guild_id ${guild_id}: ${e.response.data.message}`)
        console.error(e);
    }
};

export const setConfigurations = (configs) => ({
    type: types.SET_CONFIGURATIONS,
    payload: configs
});

export const enableLightMode = (enabled) => setConfigurations({lightMode: enabled});
export const setServerId = (serverId) => setConfigurations({serverId});
