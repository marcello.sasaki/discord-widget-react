import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { setConfigurations } from "../actions";

import { reducers } from "../reducers";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const configStore = (preloadedState) => {
    const store = createStore(reducers, preloadedState, composeEnhancers(
        applyMiddleware(thunk)
    ));

    store.dispatch(setConfigurations());

    return store;
};
