import { combineReducers } from "redux";

import { configurationsReducer } from "./configurationsReducer";
import { serverReducer, channelsReducer, membersReducer } from "./widgetDataReducer";

export const reducers = combineReducers({
    configurations: configurationsReducer,
    server: serverReducer,
    channels: channelsReducer,
    members: membersReducer
});
