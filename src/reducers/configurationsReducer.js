import { SET_CONFIGURATIONS } from "../actions/actionTypes"

const defaultServerId = process.env.REACT_APP_DISCORD_GUILD_ID;
const defaultLightModeEnabled = process.env.REACT_APP_DISCORD_THEME_LIGHT_ENABLED;

const initialConfigurations = {
    serverId: defaultServerId,
    lightMode: defaultLightModeEnabled
};

export const configurationsReducer = (configurations = initialConfigurations, action) => {
    if (action.type === SET_CONFIGURATIONS) {
        return {
            ...configurations,
            ...action.payload
        };
    }
    return configurations;
}
