import { FETCH_GUILD_WIDGET } from "../actions/actionTypes"

export const serverReducer = (server = {}, action) => {
    if (action.type === FETCH_GUILD_WIDGET) {
        return {
            id: action.payload.id,
            name: action.payload.name,
            link: action.payload.instant_invite
        };
    }

    return server;
}

export const membersReducer = (members = [], action) => {
    if (action.type === FETCH_GUILD_WIDGET) {
        return action.payload.members.map(member => ({
            id: member.id,
            name: member.username,
            game: member.game ? member.game.name : null,
            status: member.status,
            image: member.avatar_url
        }));
    }

    return members;
}

export const channelsReducer = (channels = [], action) => {
    if (action.type === FETCH_GUILD_WIDGET) {
        const channels = action.payload.channels
            .sort((c1, c2) => c1.position - c2.position)
            .map(channel => ({
                id: channel.id,
                name: channel.name,
                members: []
            }));

        action.payload.members.forEach(member => {
            if (member.channel_id) {
                let channel = channels.find(c => c.id === member.channel_id);
                if (channel) {
                    channel.members.push({
                        id: member.id,
                        name: member.username,
                        image: member.avatar_url,
                        mute: member.mute || member.self_mute,
                        deaf: member.deaf || member.self_deaf
                    });
                }
            }
        });

        return channels;
    }

    return channels;
}